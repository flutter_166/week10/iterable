import 'package:iterable/iterable.dart' as iterable;

void main(List<String> arguments) {
  Iterable<int> iterable = [1, 2, 3];
  int value = iterable.elementAt(1);
  print(value);

  //Set, List --> Iterable
  const foods = ['Salad', 'Popcorn', 'Toast', 'Lasagne', 'MamaMa'];
  Iterable<String> iterableFoods = foods;
  for(var food  in iterableFoods) {
    print(food);
  }

  print('First element is ${foods.first}');
  print('First element is ${iterableFoods.last}');

  var foundItem1 = foods.firstWhere((element) { 
    return element.length > 5;
  });
  print(foundItem1);

  var foundItem2 = foods.firstWhere((element) => element.length > 5);
  print(foundItem2);

  bool predicate(String item){
    return item.length > 5;
  }
  var foundItem3 = foods.firstWhere(predicate);
  print(foundItem3);

  var foundItem4 = 
    foods.firstWhere((item) => item.length > 10,orElse: () => 'None!');
  print(foundItem4);

  var foundItem5 =
    foods.firstWhere(
      (item) => item.startsWith('M') && item.contains('a'), 
      orElse: () => 'None!');
    print(foundItem5);

    if(foods.any((item) => item.contains('a') )) {
      print('At least on item contains "a"');
    }

    if(foods.every((item) => item.length >= 5 )) {
      print('All item have length >= 5');
    }

    var numbers = const [1, -2, 3, 42, 0, 4, 5, 6];
    var evenNumbers = numbers.where((number) => number.isEven);
    for(var number in evenNumbers) {
      print('$number is even.');
    }

    if(evenNumbers.any((number) => number.isNegative)) {
      print('eventNumbers contains negative numbers.');
    }

    var largeNumbers = evenNumbers.where((number) => number > 1000);
    if(largeNumbers.isEmpty) {
      print('largeNumbers is empty!');
    }

    var numbersUntilZero = numbers.takeWhile((number) => number != 0);
    print('Numbers until 0: $numbersUntilZero');

    var numbersStartingAtZero = numbers.skipWhile((number) => number != 0);
    print('Numbers starting at 0: $numbersStartingAtZero');

    var numbersByTwo = const [1, -2, 3, 42].map((number) => number * 2);
    print('Numbers: $numbersByTwo');

}
